<?php
class user extends Controller
{
    // Menampilkan halaman login
    public function index()
    {
        $this->loadView('auth/header');
        $this->loadView('auth/login');
        $this->loadView('auth/footer');
    }

    // Menampilkan halaman register
    public function regis()
    {
        $this->loadView('auth/header');
        $this->loadView('auth/register');
        $this->loadView('auth/footer');
    }

    // Menampilkan halaman detail akun
    public function detail()
    {
        $this->loadView('template/header');
        $this->loadView('auth/detail');
        $this->loadView('template/footer');
    }

    // Proses untuk registrasi akun
    public function regis_process()
    {
        $userModel = $this->loadModel('UserModel');
        $email = htmlspecialchars($_POST['email']);
        $fullname = htmlspecialchars($_POST['fullname']);
        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $userModel->insert($email, $fullname, $password);
        header('Location: ?c=user');
        exit;
    }

    // Proses untuk login akun
    public function login_process()
    {

        $userModel = $this->loadModel('UserModel');
        $email = htmlspecialchars($_POST['email']);
        $password = $_POST['password'];
        $user = $userModel->getByEmail($email);
        if ($user->num_rows) {
            $user = $user->fetch_object();
            if (password_verify($password, $user->password)) {
                $_SESSION['user'] = $user;
                header('Location: ?');
                exit;
            } else {
                header('Location: ?c=user');
                exit;
            }
        }
        header('Location: ?c=user');
        exit;
    }

    // Proses untuk logout akun
    public function logout()
    {
        session_destroy();
        header('Location: ?');
        exit;
    }
}
