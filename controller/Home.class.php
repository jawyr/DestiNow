<?php
class Home extends Controller
{

   public function index()
   {
      // Load model
      $placesModel = $this->loadModel('PlacesModel');
      // Get data from the model
      $places = $placesModel->getAll();
      // Memuat view dari header, index, dan footer
      $this->loadView('template/header');
      $this->loadView('home/index', [
         'places' => $places
      ]);
      $this->loadView('template/footer');
   }
}