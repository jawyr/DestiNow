<?php
class Place extends Controller
{
    // method untuk menampilkan halaman detail tempat
    public function detail()
    {
        $id = $_GET['id'];
        $id_review = $_GET['id_review'];
        // memuat PalcesModel
        $placesModel = $this->loadModel('PlacesModel');
        // cari tempat berdasarkan id
        $place = $placesModel->getById($id);
        // memuat ReviewModel
        $reviewModel = $this->loadModel('ReviewModel');
        // cari review berdasarkan id tempat
        $review = $reviewModel->getByPlace($id);
        $detailreview = null;

        // jika ada id review, cari review berdasarkan id
        if ($id_review) {
            $detailreview = $reviewModel->getById($id_review)->fetch_object();
        }
        // memuat RatingModel
        $ratingmodel = $this->loadModel('RatingModel');
        // cari rating berdasarkan id tempat
        $rating = $ratingmodel->getRatingByPlace($id);
        // retrieve nilai akhir dari rating (rata-rata/avg)
        $rating_count = (int)$rating['average_rating'];

        // memuat view yang diperlukan (header, detail, footer)
        $this->loadView('template/header');
        $this->loadView('place/detail', [
            'place' => $place->fetch_object(),
            'review' => $review,
            'detailreview' => $detailreview ? $detailreview : null,
            'rating' => $rating_count
        ]);
        $this->loadView('template/footer');
    }
}
