<?php
class Controller
{
  // Method untuk memuat model
  public function loadModel($modelName)
  {
    include_once "model/Model.class.php"; // include base model class
    include_once "model/$modelName.class.php"; // include specific model class
    return new $modelName; // membuat instance dari class model yang dimuat
  }

  // Method untuk memuat view
  public function loadView($viewName, $data = [])
  {
    foreach ($data as $var => $value) {
      $$var = $value; // membuat variable dari data yang dikirimkan
    }
    include_once "view/$viewName.php"; // include specified view file
  }
}
