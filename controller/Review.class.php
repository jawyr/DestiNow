<?php
class Review extends Controller
{
    // Method untuk meanmbahkan/post review
    public function post_review()
    {
        $id_user = $_SESSION['user']->id;
        $id_review = $_POST['id_review'];
        $id_place = $_POST['id_place'];
        $rating_count = $_POST['rating_count'];
        $review = $_POST['review'];
        $date = date('Y-m-d H:i:s');
        
        if ($id_review) {
            // update review yang telah ada
            $reviewModel = $this->loadModel('ReviewModel');
            $reviewModel->update($id_review, $rating_count, $review, $date);
            header('Location: ?c=Place&m=detail&id=' . $id_place);
        } else {
            // insert/menambahkan review baru 
            $reviewModel = $this->loadModel('ReviewModel');
            $reviewModel->insert($id_user, $id_place, $rating_count, $review, $date);
            header('Location: ?c=Place&m=detail&id=' . $id_place);
        }
        
    }

    // Method untuk menghapus review
    public function delete()
    {
        $id = $_POST['id_rating'];
        // memuat ReviewModel
        $revieModel = $this->loadModel('ReviewModel');
        // hapus review berdasarkan id
        $revieModel->delete($id);
        // redirect ke halaman detail tempat
        header('location:?c=Place&m=detail&id=' . $_POST['id_place']);
    }
}
