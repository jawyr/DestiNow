<div class="" style="width: 100%;">
    <div class="">
        <h4 class="text-center">Register Page</h4>
        <form action="?c=user&m=regis_process" method="post">
            <div class="mb-3">
                <label for="email" class="form-label">Email address</label>
                <input required type="email" name="email" class="form-control" id="email" placeholder="user@example.com">
            </div>
            <div class="mb-3">
                <label for="fullname" class="form-label">Nama Lengkap</label>
                <input required type="text" name="fullname" class="form-control" id="fullname" placeholder="Aegon Targaryen">
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input required type="password" name="password" class="form-control" id="password">
            </div>
            <div class="d-grid mb-4">
                <input value="submit" type="submit" class="btn btn-primary w-full" />
            </div>
            <p class="text-center">Sudah punya akun? <a href="/destinow/?c=user">Login di sini.</a></p>
        </form>
    </div>
</div>