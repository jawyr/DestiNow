<div style="min-height:100vh" class="pt-5">
    <div class="mt-5 d-flex justify-content-center ">
        <div class=" card" style="width: 18rem;">

            <div class="card-body">
                <h5 class="card-title"><?= $_SESSION['user']->fullname ?></h5>
                <p>Email: <?= $_SESSION['user']->email ?></p>
                <a href="?c=user&m=logout" class="btn btn-danger">Logout</a>
            </div>
        </div>
    </div>
</div>