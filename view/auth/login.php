<div class="" style="width: 100%;">
    <div class="">
        <h4 class="text-center">Login Page</h4>
        <form action="?c=user&m=login_process" method="post">
            <div class="mb-3">
                <label for="email" class="form-label">Email address</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="">
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" name="password" class="form-control" id="password" placeholder="">
            </div>
            <div class="d-grid mb-4">
                <input type="submit" value="Login" class="btn btn-primary w-full" />
            </div>
            <p class="text-center">Belum punya akun? <a href="/destinow/?c=user&m=regis">Daftar di sini.</a></p>
        </form>
    </div>
</div>