<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Website Wisata</title>
</head>
<body>
    <div class="container">
        <div class="d-flex align-items-center justify-content-center mt-5 pt-5">
            <img class="mx-auto" src="./assets/img/<?= $place->image ?>" alt="<?= $place->name ?>">
        </div>
        <h1 class="mt-2"><?= $place->name ?></h1>
        <h3> ⭐️ <?= $rating ?> / 5</h3>
        <div><?= $place->content ?></div>

        <div class="mt-5">
            <h3>Review</h3>
            <div>
                <?php foreach ($review as $row) : ?>
                    <div class="card mb-3">
                        <div class="card-body">
                            <p class="card-text font-sembi"><?= $row->review ?></p>
                            <small>rating: <?= $row->rating_count ?>/5</small>
                        </div>
                        <div class="card-footer d-flex justify-content-between align-items-center">
                            <small class="text-muted">Author: <?= $row->fullname ?> </small>
                            <?php if (isset($_SESSION['user']) && $_SESSION['user']->id === $row->id_user) : ?>
                                <div class="d-flex">
                                    <a href="?c=Place&m=detail&id=<?= $place->id ?>&id_review=<?= $row->id_rating ?>" class="btn btn-xs btn-warning">Edit</a>
                                    <form action="?c=review&m=delete" method="post">
                                        <input type="hidden" name="id_rating" value="<?= $row->id_rating ?>">
                                        <input type="hidden" name="id_place" value="<?= $_GET['id'] ?>">
                                        <input type="submit" value="Delete" class="btn btn-xs btn-danger ms-2" />
                                    </form>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="mt-5 mb-5 pb-5">
                <h5>Berikan Review</h5>
                <?php if (isset($_SESSION['user'])) : ?>
                    <form action="?c=review&m=post_review" method="post">
                        <input type="hidden" name="id_review" value="<?= $_GET['id_review'] ?>">
                        <input type="hidden" name="id_place" value="<?= $_GET['id'] ?>">
                        <div class="mb-3">
                            <label for="rating" class="form-label">Rating </label>
                            <select class="form-select" aria-label="Rating" id="rating" name="rating_count">
                                <option <?= $detailreview->rating_count === "1" ? 'selected' : '' ?> value="1">1</option>
                                <option <?= $detailreview->rating_count === "2" ? 'selected' : '' ?> value="2">2</option>
                                <option <?= $detailreview->rating_count === "3" ? 'selected' : '' ?> value="3">3</option>
                                <option <?= $detailreview->rating_count === "4" ? 'selected' : '' ?> value="4">4</option>
                                <option <?= $detailreview->rating_count === "5" ? 'selected' : '' ?> value="5">5</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="review" class="form-label">Review </label>
                            <textarea class="form-control w-full" name="review" id="review" rows="3"><?= $detailreview->review ?></textarea>
                        </div>
                        <input type="submit" value="<?= $_GET['id_review'] ? 'Update' : 'Submit' ?>" class="btn btn-primary" />
                    </form>
                <?php else : ?>
                    <p>Anda harus login untuk memberikan review, <a href="?c=user">Login disini</a></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</body>
</html>
