<div class="" style="min-height: 100vh; background-image: url(./assets/img/background-landing.jpg); background-size: cover; display: flex; align-items:center;">
    <div class="container">
        <h1 style="font-weight: bolder; font-size: 80px;">EXPLORE</h1>
        <h2>THE BEAUTY OF INDONESIA</h2>
        <p>Embark on an extraordinary journey to uncover the hidden wonders <br /> and breathtaking landscapes of Indonesia, a treasure trove for travelers.</p>
        <!--<button class="btn btn-primary btn-lg mt-4" style="border-radius: 50px;">Lets Start</button>-->
        <a href="#explore" class="btn btn-primary">Let's start!</a>
        
    </div>
</div>

<div class="container mt-5 mb-5 pb-5">
    <h2 id="explore">_________________________________________________________________________ </h2>
    <br>
    <h3>Top Travel Series</h3>
    <p>Explore our latest stories from our active users</p>
    <div class="row">
        <?php foreach ($places as $place) : ?>
            <div class="col-3">
                <div class="card" style="width: 18rem;">
                    <img src="./assets/img/<?= $place->image ?>" class="card-img-top" alt="<?= $place->name; ?>">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $place->name; ?> </h5>
                        <span class="d-flex align-items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="text-warning bi bi-star-fill" viewBox="0 0 16 16">
                                <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z" />
                            </svg> <?= (int)$place->rating ?>/5
                        </span>
                        <p class="card-text"><?= substr_replace($place->content, "...", 100) ?></p>
                        <a href="/destinow/?c=Place&m=detail&id=<?= $place->id ?>" class="btn btn-primary">Detail</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
