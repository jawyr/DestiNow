<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Explore</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
  <link rel="stylesheet" href="./assets/css/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
</head>

<body class="">
  <nav class="navbar fixed-top navbar-expand-lg " style="background-color: #F5F5F5;">
    <div class="container">
      <a class="navbar-brand" href="/destinow/">DestiNow</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/destinow/">Home</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">About Us</a>
          </li> -->

        </ul>
        <!-- check if any session -->
        <?php if (isset($_SESSION['user'])) : ?>
          <div class="d-flex">
            <p>Selamat datang, <a href="?c=user&m=detail"><?= $_SESSION['user']->fullname ?></a>!</p>
          </div>
        <?php else : ?>
          <div class="d-flex">
            <a href="?c=user" class="btn btn-primary">Login</a>
            <a href="?c=user&m=regis" class="btn btn-outline-primary ms-2">Register</a>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </nav>
  <main class="" style="overflow-x: hidden;">

  