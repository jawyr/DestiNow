<?php
class RatingModel extends Model
{
    // Melakukan perhitungan untuk mendapatkan average rating dari suatu tempat
    public function getRatingByPlace($id)
    {
        $sql = "SELECT id_place, AVG(rating_count) AS average_rating FROM rating WHERE id_place = '$id'";
        return $this->mysqli->query($sql)->fetch_assoc();
    }
}
