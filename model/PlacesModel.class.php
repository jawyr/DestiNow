<?php
class PlacesModel extends Model
{
     // Retrieve semua tempat/places
     public function getAll()
     {
          $sql = 'SELECT places.name, places.image, places.id, places.content, AVG(rating.rating_count) as rating FROM places LEFT JOIN rating ON rating.id_place = places.id GROUP BY places.id';
          $result = $this->mysqli->query($sql);

          // Mengecek apakah query berhasil dijalankan
          if ($result) {
               $data = array();
               while ($row = $result->fetch_object()) {
                    $data[] = $row; // menambahkan tiap baris sebagai object ke data array
               }
               $result->close();

               return $data; // mengembalikan data array dari tempat/places
          }
          return []; // mengembalikan array kosong jika tidak ada data
     }

     // Retrieve tempat/places berdasarkan id places
     public function getById($id)
     {
          $sql = "SELECT * FROM places WHERE id = $id";
          return $this->mysqli->query($sql);
     }
}
