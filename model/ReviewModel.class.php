<?php
class ReviewModel extends Model
{
    // Get all data from table rating
    public function getAll()
    {
        $sql = 'SELECT * FROM rating ORDER BY date DESC';
        return $this->mysqli->query($sql);
    }

    // Retrive rating berdasarkan id place
    public function getByPlace($id)
    {
        $sql = "SELECT rating.id as id_rating, rating.id_place, rating.id_user, rating.rating_count, rating.review, rating.date, users.* FROM rating JOIN users on users.id = rating.id_user WHERE id_place = '$id' ORDER BY date DESC";
        $result = $this->mysqli->query($sql);

        if ($result) {
            $data = array();
            while ($row = $result->fetch_object()) {
                $data[] = $row;
            }
            $result->close();

            return $data;
        }
        return [];
    }

    // Insert/menambahkan rating baru
    public function insert($id_user, $id_place, $rating_count, $review, $date)
    {
        $sql = "INSERT INTO rating (id_user, id_place, rating_count, review, date) 
        VALUES ('$id_user', '$id_place', '$rating_count', '$review', '$date')";
        $this->mysqli->query($sql);
    }

    // Retrieve rating berdasarkan id rating
    public function getById($id)
    {
        $sql = "SELECT * FROM rating WHERE id = $id";
        return $this->mysqli->query($sql);
    }

    // Update rating 
    public function update($id,  $rating_count, $review, $date)
    {
        $sql = "UPDATE rating SET rating_count = '$rating_count', review = '$review', date = '$date' WHERE id = '$id'";
        $this->mysqli->query($sql);
    }

    // Menghapus rating
    public function delete($id)
    {
        $sql = "DELETE FROM rating WHERE id = $id";
        $this->mysqli->query($sql);
    }
}
