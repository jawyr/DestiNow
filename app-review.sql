-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 20, 2023 at 07:19 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `app-review`
--

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(50) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `name`, `image`, `content`) VALUES
(1, 'Pura Besakih', 'pura.jpeg', 'Pura Agung Besakih adalah pura terbesar dan termegah di Bali. Pulau ini terletak di Desa Besakih, Kecamatan Rendang, berada di lereng sebelah barat daya Gunung Agung, gunung tertinggi di Bali.\r\n\r\nPura Agung Besakih memiliki gaya arsitektur yang mengagumkan khas Bali dan berada di ketinggian 915 kaki di kaki Gunung Agung dengan memukau. Bangunan yang dibangun sejak abad ke-10 Masehi ini menjadi pusat kegiatan spiritual Hindu Dharma di Pulau Dewata.\r\n\r\nDengan segala fitur yang dimiliki Pura Besakih, tidak aneh jika situs ini ditetapkan sebagai Situs Warisan Budaya UNESCO sejak selamat dari erupsi Gunung Agung pada tahun 1963.\r\n\r\nAkses dari Kota Denpasar untuk mencapai tempat ini berjarak sekitar 25 km ke arah utara dari Kota Semarapura, Kabupaten Klungkung. Perjalanan menuju Pura Besakih melewati panorama Bukit Jambul yang juga merupakan salah satu obyek dan daya tarik wisata Kabupaten Karangasem.'),
(2, 'Kepulauan Derawan', 'derawan.jpeg', 'Kepulauan Derawan adalah sebuah kepulauan yang berada di Kabupaten Berau, Kalimantan Timur. Di kepulauan ini terdapat sejumlah objek wisata bahari menawan, salah satunya Taman Bawah Laut yang diminati wisatawan mancanegara terutama para penyelam kelas dunia.\r\n\r\nSaat pertama kali menginjakan kaki disini, jangan heran bila Toppers akan disambut dengan hamparan pasir putih yang mempesona. Warna airnya yang sangat jernih juga akan bikin kamu betah untuk berlama-lama di kawasan ini. Panorama alam ini menjadi sajian liburan utama bagi wisatawan dalam berkunjung\r\n\r\nFaktor lain yang menjadi alasan mengapa kawasan ini wajib untuk dikunjungi, yaitu ekosistem bawah laut dan keasrian alam sekitarnya benar-benar masih sangat terjaga. Wajar jika banyak wisatawan yang datang berbondong-bondong ke tempat ini, baik wisatawan lokal maupun wisatawan internasional.\r\n\r\nPada tahun 2005 pemerintah telah mencoba mendaftarkan kawasan wisata ini ke UNESCO sebagai salah satu situs warisan dunia, bahkan sampai mendapat julukan sebagai “Pristine Island”.'),
(3, 'Taman Nasional Bunaken', 'taman-nasional-bunaken.jpg', 'Taman Nasional Bunaken adalah surga bawah laut yang mempesona dan tak terlupakan. Keindahan alam, keanekaragaman hayati, dan usaha konservasi yang dilakukan menjadikan tempat ini sebagai tempat wisata yang tak boleh dilewatkan bagi para penyelam dan pecinta alam. Nikmati keajaiban bawah laut dan saksikan kehidupan laut yang penuh keindahan di Taman Nasional Bunaken.\r\n\r\nKetika menyelam di Taman Nasional Bunaken, pengunjung akan disambut oleh panorama bawah laut yang memukau. Terumbu karang yang indah dan warna-warni membentuk rumah bagi berbagai spesies ikan, biota laut, dan tumbuhan karang yang beragam. Pengunjung dapat melihat ikan karang, pari manta, penyu laut, dan berbagai spesies lainnya yang menghuni perairan ini. Kejernihan air dan keindahan terumbu karang yang utuh membuat setiap penyelaman menjadi pengalaman yang tak terlupakan.\r\n\r\nSelain menyelam, pengunjung juga dapat menikmati aktivitas snorkeling di Taman Nasional Bunaken. Dengan menggunakan masker dan snorkel, Anda dapat menyelam ke permukaan air dan menyaksikan pemandangan bawah laut yang menakjubkan. Terumbu karang yang berwarna-warni dan ikan-ikan yang berenang dengan riang di sekitarnya akan memukau mata Anda. Pantai-pantai yang indah di sekitar taman nasional ini juga merupakan tempat yang sempurna untuk bersantai, berjemur, dan menikmati panorama laut yang menyejukkan.\r\n\r\nTaman Nasional Bunaken, terletak di Sulawesi Utara, adalah surga bagi penyelam dan pecinta kehidupan bawah laut. Dengan keindahan alam yang menakjubkan, terumbu karang yang spektakuler, dan keanekaragaman hayati laut yang luar biasa, tempat ini menjadi salah satu destinasi penyelaman terbaik di Indonesia. Taman nasional ini meliputi wilayah seluas sekitar 89.065 hektar, dengan lebih dari 20 titik penyelaman yang menawarkan pengalaman yang luar biasa.'),
(4, 'Pantai Parai Tenggiri', 'pantai-parai.jpeg', 'Pasti diantara kamu sudah pernah menyaksikan film populer Laskar Pelangi yang berlatar di Pulang Belitung, bukan? Selain alur ceritanya yang menarik, lokasi film ini juga banyak menyita perhatian penonton. \r\n\r\nBerbeda dengan pantai lain pada umumnya, Parai Tenggiri memiliki struktur pantai yang landai dengan air laut berwarna hijau toska serta pasir putihnya yang lembut. Ombak di pantai ini juga tenang sehingga menjadi salah satu alasan yang menarik bagi wisatawan yang senang berenang. \r\n\r\nTidak hanya berenang, kamu juga bisa menikmati aktivitas memancing, parasailing, menyelam, snorkeling, dan masih banyak lainnya.'),
(5, 'Taman Nasional Komodo', 'komodo.jpg', 'Perjalanan di Taman Nasional Komodo akan membawa Anda ke habitat asli komodo. Pengunjung dapat melihat komodo dalam lingkungan alaminya dan mempelajari kehidupan mereka yang unik. Pulau Komodo dan Pulau Rinca adalah dua pulau utama yang menjadi tempat tinggal bagi populasi komodo yang terbesar. Jalan-jalan trekking yang tersedia akan membawa Anda melintasi padang rumput kering, hutan kering, dan bukit-bukit yang menakjubkan, sambil mencari tanda-tanda keberadaan komodo. Melihat komodo secara langsung adalah pengalaman yang luar biasa dan memberikan pemahaman yang lebih mendalam tentang pentingnya konservasi spesies langka ini.\r\n\r\nTaman Nasional Komodo juga memiliki keberagaman ekosistem yang luar biasa. Dari hutan tropis hingga hutan mangrove, dari gunung berapi hingga padang rumput, setiap sudut taman nasional ini menawarkan pemandangan alam yang menakjubkan. Tidak hanya komodo, pengunjung juga dapat melihat berbagai spesies satwa liar lainnya, seperti rusa Timor, babi hutan, dan berbagai spesies burung endemik. Taman Nasional Komodo adalah kombinasi sempurna antara keindahan alam, konservasi, dan pengalaman bertemu dengan hewan purba yang mengagumkan.\r\n\r\nMengunjungi Taman Nasional Komodo adalah sebuah petualangan yang menakjubkan yang akan memberikan Anda wawasan mendalam tentang keanekaragaman hayati dan keindahan alam Indonesia. Dari bertemu dengan komodo, menjelajahi pulau-pulau eksotis, hingga snorkeling di perairan yang indah, setiap momen di taman nasional ini akan menghadirkan pengalaman yang tak terlupakan. Taman Nasional Komodo adalah tujuan wisata yang menggabungkan petualangan, keindahan alam, dan pengenalan terhadap spesies langka yang sangat menarik bagi para pengunjung.\r\n\r\nTaman Nasional Komodo, yang terletak di provinsi Nusa Tenggara Timur, adalah rumah bagi hewan purba yang ikonis, yaitu komodo, yang merupakan spesies kadal terbesar di dunia. Taman nasional ini juga dikenal karena keindahan alamnya yang spektakuler, pulau-pulau eksotis, dan keanekaragaman hayati laut yang luar biasa. Mengunjungi Taman Nasional Komodo adalah sebuah petualangan yang tak terlupakan dan pengalaman yang mendalam di dunia hewan dan alam.'),
(6, 'Candi Borobudur', 'borobudur.jpg', 'Candi Borobudur, sebuah keajaiban arsitektur di Magelang, Jawa Tengah, adalah salah satu situs budaya paling terkenal di Indonesia dan juga merupakan situs warisan dunia UNESCO. Candi ini menjadi tujuan wisata yang tak terlupakan bagi pengunjung dari seluruh dunia yang tertarik pada sejarah, budaya, dan spiritualitas.\r\n\r\nDibangun pada abad ke-8 oleh dinasti Syailendra, Candi Borobudur menggambarkan kehidupan Buddha dan filsafat Mahayana. Arsitektur candi ini sangat mengesankan, dengan bangunan yang megah dan kompleks yang terdiri dari sembilan tingkat, yang terdiri dari tiga tingkat bawah tanah (Kamadhatu), empat tingkat di tengah (Rupadhatu), dan dua tingkat di puncak (Arupadhatu). Setiap tingkat dihiasi dengan relief yang rumit dan panel-panel ukiran yang menggambarkan ajaran Buddha, cerita-cerita epik, dan kehidupan sehari-hari pada masa itu.\r\n\r\nMenjelajahi kompleks Candi Borobudur adalah sebuah pengalaman yang mengesankan. Saat Anda naik melalui anak tangga yang berliku, Anda akan merasakan atmosfir keheningan dan ketenangan yang terasa sangat spiritual. Di setiap tingkat, pengunjung dapat mengamati dan menghayati relief-relief yang bersejarah dengan seksama, mengikuti perjalanan spiritual dari dunia material hingga mencapai keadaan nirwana.\r\n\r\nSaat matahari terbit atau terbenam, pemandangan Candi Borobudur semakin menakjubkan. Cahaya matahari yang memancar dari balik gunung menjadikan candi ini tampak memancarkan aura magis. Suasana yang tenang dan pemandangan alam yang indah menambah keajaiban tempat ini.\r\n\r\nCandi Borobudur adalah sebuah permata budaya yang harus dikunjungi oleh setiap wisatawan yang datang ke Indonesia. Keindahan arsitektur, kedalaman sejarah, dan suasana spiritual yang ada di tempat ini membuatnya menjadi salah satu destinasi yang paling menarik dan menginspirasi di dunia.'),
(8, 'Nusa Dua (Bali)', 'nusa-dua.jpeg', 'Pulau Seribu Dewa satu ini memang tidak perlu diragukan lagi terkait keindahan dan pesonanya dalam memikat para wisatawan dalam negeri maupun mancanegara. Di Bali, ada satu tempat wisata yang begitu cantik, yakni Nusa Dua. \r\n\r\nObjek wisata pantai ini memiliki pasir putih yang lembut dan air laut yang berwarna biru jernih. Kamu akan dimanjakan dengan berbagai fasilitas saat berkunjung ke tempat satu ini. Mulai dari penginapan dan resort yang berkelas, restoran, pusat perbelanjaan, hingga aktivitas berselancar di pantainya.');

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_place` int(11) NOT NULL,
  `rating_count` int(11) NOT NULL,
  `review` text NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `id_user`, `id_place`, `rating_count`, `review`, `date`) VALUES
(1, 4, 1, 4, 'asasa', '2023-06-10'),
(9, 5, 1, 4, 'asas', '2023-06-10'),
(10, 5, 2, 5, 'asasa', '2023-06-10'),
(11, 7, 1, 3, 'wow this is a good place', '2023-06-10'),
(12, 7, 3, 5, 'apakah kita dapat way kambas', '2023-06-10'),
(18, 10, 1, 2, 'jelek', '2023-06-15'),
(24, 19, 6, 5, 'good place', '2023-06-20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `fullname`, `password`) VALUES
(4, 'user@gmail.com', 'Ahmad Saynusi', '$2y$10$C059QJ5a2fxdtherZG9goOlXZz56nJ22NWbwqs2hUS6m5DN9J.GFW'),
(5, 'user2@gmail.com', 'user 2', '$2y$10$51.2zhbO.QxVkfgzJiUmm.WXYj/fX8vVxUv7RvxeUOTnuvEkqHrCO'),
(6, 'romifajar347@student.ub.ac.id', 'Romy', '$2y$10$lWv4pwxBLo4lNvBwBAi2xO0q6XIU9Jtf6VCoMq8hRjauhLBfNvgIW'),
(7, 'romifajar77@student.ub.ac.id', 'Romyy', '$2y$10$BPP1kfttjMe4hLogJNOWmuRMeaWQxO5SqhEsRni3XlFOxJIrAhcWa'),
(8, 'testing@mail.com', 'mail', '$2y$10$KujmH9XCudTgZNZZKReCbu.OzEfI0SWoiI0E/I/wCecbr.R10P0AS'),
(9, 'romi@mail.com', 'audrey', '$2y$10$6pDS6gYNj81sj7Kla4YymOzk7UFyH4O31235idS.JkRqrzGX9HKE6'),
(10, 'yuslama@stuedent.ub.ac.id', 'ggg', '$2y$10$bQI3xoDYXQncw70u8V05zOG8l3JU6RnZB1NfeHMekvd5XwIVzqraS'),
(12, '', '', '$2y$10$DPk6BIWhB/tnOd4x/DtG5u.HFenUCTOE7ynA98LnF9Ie6rx1gzKyq'),
(14, 'testingiiu2@mail.com', 'testing', '$2y$10$HYQ0V/fmKxHdM9r8wiO4Bexo85iFlRf4ptOD0nsXLTpWBsPMAoNoO'),
(15, 'test123@mail.com', 'testing123', '$2y$10$uUrzisN8h5uiNidAw3bShOtURjTz8ag3hZF/gbi/S63xxk1Yb.nhm'),
(16, 'test21@mail.com', 'testing', '$2y$10$Gj8Z7XhNzmZAktsUtm0pOuJ/RZWq1L3yfxhTlmn4bFqRdwchxYXC.'),
(19, 'testing1@mail.com', 'John', '$2y$10$DWBDShRmLDH9WG8hDwHlzOt15dmHnFyln58jtqxQsAJrmF8HFF6/W');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_place` (`id_place`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `rating_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `rating_ibfk_2` FOREIGN KEY (`id_place`) REFERENCES `places` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
